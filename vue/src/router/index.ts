import { createRouter,createWebHistory,RouteRecordRaw } from "vue-router";
const Login = ()=>import('../views/zmx/login.vue')
const Home = ()=>import('../views/main/home.vue')
const Message = ()=>import('../views/main/message.vue')
const My = ()=>import('../views/main/my.vue')
const Zong = ()=>import('../views/main/zong.vue')
const routes: Array<RouteRecordRaw> = [
    {path:'/',name:'login',component:Login},
    {path:'/zong',name:'zong',component:Zong,
     children:[
        {path:'/home',name:'home',component:Home,},
        {path:'/message',name:'message',component:Message,},
        {path:'/my',name:'my',component:My}
     ]
},
]
const router=createRouter({
    history:createWebHistory(),
    routes
})
export default router