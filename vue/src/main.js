import { createApp } from 'vue'
import { Tabbar, TabbarItem } from 'vant';
import 'vant/lib/index.css'
// import './style.css'
import App from './App.vue'
import router from './router/index'

// createApp(App).mount('#app')
const app = createApp(App).use(router).mount('#app')

